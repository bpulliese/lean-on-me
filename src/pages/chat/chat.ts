import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, MenuController, NavController, NavParams} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {AuthenticationProvider} from "../../providers/authentication/authentication";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import {Avatar, User} from "../../shared/models/user.model";
import {AppProvider} from "../../providers/app/app";
import {ChatSessionPage} from "../chat-session/chat-session";
import {Session} from "../../shared/models/session.model";
import {ChatSessionProvider} from "../../providers/chat-session/chat-session";
import {MatchFinderProvider} from "../../providers/match-finder/match-finder";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  private peersCollection: AngularFirestoreCollection<any>;
  private sessionsCollection: AngularFirestoreCollection<any>;
  peers: User[] = [];
  sessions: Session[] = [];
  loader: Loading;
  private _matchedNickname:string;
  get matchedNickname(): string {
    return this._matchedNickname;
  };
  set matchedNickname(v:string){
    this._matchedNickname = v;
  }
  constructor(private nav: NavController,
              private authService: AuthenticationProvider,
              public navParams: NavParams,
              private menu: MenuController,
              private afs: AngularFirestore,
              private appService: AppProvider,
              private chateSessionService: ChatSessionProvider,
              private matchFinderService: MatchFinderProvider,
              public loadingCtrl: LoadingController) {

    menu.enable(true);
    this.peersCollection = afs.collection<User[]>('peers');
    this.sessionsCollection = afs.collection<Session[]>('chat-sessions');
    this.peersCollection
      .valueChanges()
      .subscribe((peers:User[])=>{
        this.peers = [];
        for (let peer of peers) {
          if(this.authService.loggedInUser && peer.email!==this.authService.loggedInUser.email) {
            this.peers.push(new User(peer));
          }
        }
        if(this.peers && this.peers.length > 0 && authService.loggedInUser!=null)
        {
          this.showLoading();
          this.matchFinderService
            .findMatch(this.peers, authService.loggedInUser)
            .subscribe((matchedNickname: string) => {
              this.matchedNickname = matchedNickname;
              this.loader.dismiss();
            },
              (error)=>{
              this.loader.dismiss();
              });
        }
        console.log(this.peers);
      });

    this.sessionsCollection
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      }).subscribe((sessions: Session[])=>{
        console.log(sessions);
        this.sessions = sessions;
      });

  }

  ngOnInit(){
     if(!this.authService.isUserLoggedIn()){
      this.nav.setRoot(LoginPage);
       this.menu.close();
     }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

  getAvatarImg(e:Avatar){
    return this.appService.getAvatarByEnum(e);
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Hang tight, we're finding the best peer match for you..."
    });
    this.loader.present();
  }

  findAnActiveSession(peer):Session{
    let foundSession;
    for (let i=0; i<this.sessions.length;i++){
      let foundCount = 0;
      for(let x=0;x<this.sessions[i].participants.length;x++){
        if(this.sessions[i].participants[x].email===peer.email || this.sessions[i].participants[x].email===this.authService.loggedInUser.email){
          foundCount++;
        }
        if(foundCount==2){
          foundSession = this.sessions[i];
          break;
        }
      }

    }
     let session = new Session();
     if (foundSession){
       session.id = foundSession.id;
       session.participants = foundSession.participants;
     }
     return session;
  }


  startChat(peer) {

    let foundSession = this.findAnActiveSession(peer);

    if(typeof foundSession.id === 'undefined') {
      let session = new Session();
      session.participants = [Object.assign({},new User(this.authService.loggedInUser)), Object.assign({},peer)];

      // set online status
      this.afs.collection("chat-sessions").add(Object.assign({}, session))
        .then((doc) => {
          console.log("Chat Session Created", doc);
          session.id = doc.id;
          this.chateSessionService.activeSession = session;
          this.nav.push(ChatSessionPage);
        })
        .catch(function (error) {
          console.error("Error writing document: ", error);
        });
    }else{
      this.chateSessionService.activeSession = foundSession;
      this.nav.push(ChatSessionPage);
    }
  }
}
