import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, ToastController} from 'ionic-angular';
import {AuthenticationProvider} from "../../providers/authentication/authentication";
import {Avatar, IUser, User} from "../../shared/models/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ChatPage} from "../chat/chat";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectedAvatar:Avatar;
  form: FormGroup;
  submitted: boolean;
  userFormModel:User = new User();
  loader: Loading;
  currentStep: string;

  formErrors = {
    'firstname': '',
    'surname': '',
    'nickname':'',
    'avatar': '',
    'anonymous': '',
    'email': '',
    'password': '',
    'realpeeps': '',
    'chatbot': '',
    'hcp': '',
    'stresstypes': '',
    'martialstatus': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'Firstname is required'
    },
    'surname': {
      'required':      'Surname is required'
    },
    'nickname': {
      'required':      'Nickname is required'
    },
    'avatar': {
      'required':      'Avatar is required'
    },
    'anonymous': {
      'required':      'Anonymous toggle is required'
    },
    'email': {
      'required':      'Email is required'
    },
    'password': {
      'required':      'Password is required'
    },
    'realpeeps': {
      'required':      'Reel Peeps is required'
    },
    'chatbot': {
      'required':      'Chatbot is required'
    },
    'hcp': {
      'required':      'hcp is required'
    },
    'stresstypes': {
      'required':      'Stress type is required'
    },
    'maritalstatus': {
      'required': 'Marital status required'
    }
  };

  constructor(private nav: NavController,
              private authService: AuthenticationProvider,
              private fb: FormBuilder,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController) {
  }

  ngOnInit() {
    this.currentStep = 'step1';
    this.selectedAvatar = Avatar.Dog;
    this.buildForm();
  }



  isStepDisabled(step:string){
    switch(step){
      case 'step1':
        return false;
      case 'step2':
        return !(this.form.get('firstname').valid &&
          this.form.get('surname').valid &&
          this.form.get('nickname').valid &&
          this.form.get('maritalstatus').valid);
      case 'step3':
        return !(this.form.get('avatar').valid &&
          this.form.get('email').valid &&
          this.form.get('password').valid);
    }
  }



  register(u:User){
    this.showLoading();

    this.authService
      .register(new User(u))
      .then((result)=>{
        if (result) {
         // this.nav.setRoot('ChatPage');
          //this.nav.push(OtherPage);
          this.showConfirmationToast();
          this.nav.setRoot(ChatPage);
          this.loader.dismiss();
        }
    })
      .catch((e)=>{
        console.error(e);
        this.loader.dismiss();
      });
  }

  showConfirmationToast() {
    let toast = this.toastCtrl.create({
      message: 'You have been registered successfully',
      duration: 3000
    });
    toast.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Almost done, your account is being created..."
    });
    this.loader.present();
  }

  buildForm(): void {
    this.form = this.fb.group({
      'firstname': [this.userFormModel.firstname, Validators.required],
      'surname': [this.userFormModel.surname, Validators.required],
      'nickname': [this.userFormModel.nickname, Validators.required],
      'avatar': [this.userFormModel.avatar, Validators.required],
      'anonymous': [this.userFormModel.anonymous],
      'email': [this.userFormModel.email, [Validators.required,Validators.email]],
      'password': [this.userFormModel.password, Validators.required],
      'realpeeps': [this.userFormModel.realpeeps],
      'chatbot': [this.userFormModel.chatbot],
      'hcp': [this.userFormModel.hcp],
      'stresstypes': [this.userFormModel.stresstypes, Validators.required],
      'maritalstatus': [this.userFormModel.maritalstatus, Validators.required],
      'sex': [this.userFormModel.sex],
      'age': [this.userFormModel.age]
    });
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.form) { return; }
    const form = this.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }


  gotoStep(step){
    this.currentStep = step;
  }

  submit($event): void {
    if (this.form.valid) {
      this.submitted = true;
      this.userFormModel = this.form.value;
      this.register(this.userFormModel);
    }
  }

}
