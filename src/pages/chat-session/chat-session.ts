import {Component, ElementRef, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ChatSessionProvider} from "../../providers/chat-session/chat-session";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import {AuthenticationProvider} from "../../providers/authentication/authentication";
import {Observable} from "rxjs/Observable";
import {Session} from "../../shared/models/session.model";
import {Avatar, User} from "../../shared/models/user.model";
import {AppProvider} from "../../providers/app/app";
import {ReccomendationsProvider} from "../../providers/reccomendations/reccomendations";
import {ReccomendationModel} from "../../shared/models/reccomendation.model";
import {MessageModel} from "../../shared/models/message.model";

/**
 * Generated class for the ChatSessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-session',
  templateUrl: 'chat-session.html'
})
export class ChatSessionPage {

  @ViewChild('scrollContent') private scrollContainer: ElementRef;
  @ViewChild(Content) content: Content;
  private messagesCollection: AngularFirestoreCollection<any>;
  private messages$:Observable<any>;
  session: Session;
  editorMsg: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public chatSessionService: ChatSessionProvider,
              private afs: AngularFirestore,
              private authService: AuthenticationProvider,
              private appService: AppProvider,
              private reccomendationsService: ReccomendationsProvider) {
    this.session = this.chatSessionService.activeSession;
    this.messagesCollection = afs.collection('chat-sessions')
      .doc(this.session.id)
      .collection<MessageModel>('messages',ref => ref.orderBy('date'));
    this.messages$ = this.messagesCollection.valueChanges();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatSessionPage');
    this.content.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.content.scrollToBottom();
  }

  getPeerNickname(session:Session){
    let peer;
    for(let participant of session.participants) {
      if(participant.email!==this.authService.loggedInUser.email){
        peer = participant;
      }
    }
    return peer.nickname;
  }

  isLoggedInUser(user:User): boolean{
    return (user.email === this.authService.loggedInUser.email)
  }

  getAvatarImg(e:Avatar) {
    return this.appService.getAvatarByEnum(e);
  }

  sendMsg(){
    let message: MessageModel = {
      msg: this.editorMsg,
      peer: this.authService.loggedInUser,
      date: Date.now(),
      reccomendations:  this.hasReccomendations(this.editorMsg)
    };
    this.afs.collection("chat-sessions")
      .doc(this.session.id)
      .collection("messages")
      .add(Object.assign({},message))
      .then((doc) => {
        console.log("Chat msg sent", doc);
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });

    //clear field
    this.editorMsg = '';
  }


  private hasReccomendations(msg:string): ReccomendationModel[]{
    let flatReccomendations = [];
    let recs = this.reccomendationsService.checkForReccomendation(msg);
    for(let rec of recs){
      flatReccomendations.push(Object.assign({},rec));
    }
    return (flatReccomendations.length)? flatReccomendations : [];
  }

}
