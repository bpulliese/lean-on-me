import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatSessionPage } from './chat-session';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    ChatSessionPage
  ],
  imports: [
    IonicPageModule.forChild(ChatSessionPage),
    PipesModule
  ],
})
export class ChatSessionPageModule {}
