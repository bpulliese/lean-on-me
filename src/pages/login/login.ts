import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {IUser} from '../../shared/models/user.model';
import {AuthenticationProvider} from "../../providers/authentication/authentication";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as IUser;
  showLogo = true;

  constructor(private afAuth: AngularFireAuth,
              public navCtrl: NavController,
              public navParams: NavParams,
              private authService: AuthenticationProvider) {
  }

  login(user: IUser) {
      this.authService.login(user);
    }

  register() {
    this.navCtrl.setRoot('HomePage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.authService.authenticationComplete.subscribe((user)=>{
      this.navCtrl.setRoot('ChatPage');
    });
  }

}
