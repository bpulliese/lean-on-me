import {Component, ViewChild} from '@angular/core';
import {MenuController, NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {LoginPage} from "../pages/login/login";
import {ChatPage} from "../pages/chat/chat";
import {AuthenticationProvider} from "../providers/authentication/authentication";
import {ProfilePage} from "../pages/profile/profile";

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  @ViewChild('content') nav: NavController;
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private authService: AuthenticationProvider, private menu: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  logout() {
    this.authService.logout().then((result)=>{
      this.authService.loggedInUser = null;
        this.nav.setRoot(LoginPage);
        this.menu.close();
    });
  }

  openChat = () =>{
    this.nav.setRoot(ChatPage);
    this.menu.close();
  }

  openProfile = () =>{
    this.nav.setRoot(ProfilePage);
    this.menu.close();
  }

}
