import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuthModule} from "angularfire2/auth";
import {LoginPage} from "../pages/login/login";
import {HomePageModule} from "../pages/home/home.module";
import {LoginPageModule} from "../pages/login/login.module";
import { AppProvider } from '../providers/app/app';
import {ChatPageModule} from "../pages/chat/chat.module";
import { AuthenticationProvider } from '../providers/authentication/authentication';
import {HttpClientModule} from "@angular/common/http";
import {AngularFirestoreModule} from "angularfire2/firestore";
import {ProfilePageModule} from "../pages/profile/profile.module";
import {ReactiveFormsModule} from "@angular/forms";
import {ChatSessionPageModule} from "../pages/chat-session/chat-session.module";
import { ChatSessionProvider } from '../providers/chat-session/chat-session';
import {PipesModule} from "../pipes/pipes.module";
import { MatchFinderProvider } from '../providers/match-finder/match-finder';
import { ReccomendationsProvider } from '../providers/reccomendations/reccomendations';

export const firebaseConfig = {
  apiKey: "AIzaSyDAL7Kirjm7XGJTvqcAwI_cYj_Jm6BdzvA",
  authDomain: "lean-on-me-6313f.firebaseapp.com",
  databaseURL: "https://lean-on-me-6313f.firebaseio.com",
  projectId: "lean-on-me-6313f",
  storageBucket: "lean-on-me-6313f.appspot.com",
  messagingSenderId: "910827095902"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      mode: 'md'
  }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    LoginPageModule,
    HomePageModule,
    ChatPageModule,
    ProfilePageModule,
    ReactiveFormsModule,
    ChatSessionPageModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppProvider,
    AuthenticationProvider,
    ChatSessionProvider,
    MatchFinderProvider,
    ReccomendationsProvider
  ]
})

export class AppModule {}
