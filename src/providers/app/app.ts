import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Avatar} from "../../shared/models/user.model";

/*
  Generated class for the AppProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AppProvider Provider');
  }

  getAvatarByEnum(e:Avatar){
    switch(e){
      case Avatar.Dog:
        return '../assets/imgs/avatars/dog.png';
      case Avatar.Dog2:
        return '../assets/imgs/avatars/dog2.jpeg';
      case Avatar.Cat:
        return '../assets/imgs/avatars/cat.jpg';
      case Avatar.Bird:
        return '../assets/imgs/avatars/bird.jpeg';
      default:
        return '../assets/imgs/avatars/cat.jpg';
    }
  }

}
