import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Session} from "../../shared/models/session.model";

/*
  Generated class for the ChatSessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChatSessionProvider {

  activeSession: Session;

  constructor(public http: HttpClient) {
    console.log('Hello ChatSessionProvider Provider');
  }

}
