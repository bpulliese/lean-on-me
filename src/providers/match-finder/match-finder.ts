import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser, StressType } from '../../shared/models/user.model';
import { Observable } from 'rxjs/Observable';
import { matchFinderRequest } from '../../shared/models/matches.model';

/*
  Generated class for the MatchFinderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MatchFinderProvider {
  matchingFunctionUrl = "https://md-leanonme.azurewebsites.net/api/findmatch";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': 'https://md-leanonme.azurewebsites.net/api/findmatch',
      'Access-Control-Allow-Methods': 'POST, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
      'Access-Control-Allow-Credentials':  'true'
    })
  };

  constructor(public http: HttpClient) {
    console.log('Hello MatchFinderProvider Provider');
  }

  findMatch(peers:IUser[], currentUser:IUser):Observable<string>
  {
    // 1) Age, 2) Maritial Status, 3) Blue Collar, 4) Industry, 5) Financial Stress, 6) Family Stress, 7) Relationship Stress
    let request : any = {
      user: [
        +currentUser.age,
        currentUser.maritalstatus,
        0,
        0,
        1,//currentUser.stresstypes.indexOf(StressType.Financials) > -1,
        1,//currentUser.stresstypes.indexOf(StressType.Parenthood) > -1,
        0,//currentUser.stresstypes.indexOf(StressType.Bullying) > -1,
        1,
        1,
        0

      ],
      peers: {}
    }

    peers.forEach(peer => {
      request.peers[peer.nickname] = [
        +peer.age,
        peer.maritalstatus,
        0,
        0,
        peer.stresstypes.indexOf(StressType.Financials) > -1 ? 1 : 0,
        peer.stresstypes.indexOf(StressType.Parenthood) > -1? 1 : 0,
        peer.stresstypes.indexOf(StressType.Bullying) > -1? 1 : 0,
        peer.stresstypes.indexOf(StressType.SocialIsolation) > -1? 1 : 0,
        peer.stresstypes.indexOf(StressType.WorkRelatedStress) > -1? 1 : 0,
        peer.stresstypes.indexOf(StressType.DontKnow) > -1? 1 : 0
      ];
    });
    console.log(request);
    return this.http.post<string>(this.matchingFunctionUrl, request, this.httpOptions)

  }
}
