import { HttpClient } from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {IUser, User} from "../../shared/models/user.model";
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFirestore} from "angularfire2/firestore";
import {ToastController} from "ionic-angular";

/*
  Generated class for the AuthenticationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationProvider {

  loggedInUser: User = null;
  authenticationComplete: EventEmitter<User> = new EventEmitter();

  constructor(
    public http: HttpClient,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    public toastCtrl: ToastController) {
    }

  isUserLoggedIn(){
    return (this.loggedInUser !== null);
  }

  login(user: IUser, peer?:IUser): Promise<any> {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then((result)=>{

        this.loggedInUser = new User(user);

        // set online status
        if(peer) {
           this.setOnlineStatus(user,peer);
        }else{
          this.afs.collection('profiles')
            .doc(user.email).valueChanges()
            .subscribe((profile)=>{
              return this.setOnlineStatus(user,profile);
            })
        }

        return result;
      });
  }

  logout(): Promise<any> {
    return this.afs.collection("peers").doc(this.loggedInUser.email).delete()
      .then(()=>{
        return this.afAuth.auth.signOut();
      });
  }

  register(user: User): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      ).then((result)=>{

        // save credentials
       let credentials = new User(user),
       flattenedUserModel = Object.assign({},user);

       console.log(flattenedUserModel);

        //remove password from saving along with profile
        delete user.password;

        return this.afs.collection("profiles")
          .doc(user.email)
          .set(flattenedUserModel)
          .then(()=>{

            // auto login user after successful registration
            return this.login(credentials,flattenedUserModel)
              .then((result)=>{
              return result;
            });
          })
          .catch(function(error) {
            console.error("Error writing document: ", error);
          });
    }).catch((error)=>{
        let toast = this.toastCtrl.create({
          message: error.message,
          duration: 3000
        });
        toast.present();

        return error;
    });
  }


  private setOnlineStatus(user,peer){
    this.afs.collection("peers").doc(user.email).set(Object.assign({},peer))
      .then( () => {
        console.log("Peer successfully flagged online");

        // set logged in user
        this.loggedInUser = peer;

        this.authenticationComplete.emit(peer);
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  }

}
