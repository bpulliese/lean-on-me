import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ReccomendationModel} from "../../shared/models/reccomendation.model";

/*
  Generated class for the ReccomendationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReccomendationsProvider {

  private reccomendationsMap = [
    {
      name: 'holiday',
      reccomendation: new ReccomendationModel({img: '../assets/partners/flightcenter.jpg', content: "Hey!, maybe you should take a break. Here's a 35% discount, from our partners at Flight center."}),
      words: ["need a break", "exhausted", "over it", "tired"]
    },
    {
      name: 'gym',
      reccomendation: new ReccomendationModel({img: '../assets/partners/fitnessfirst.jpg', content: "Hey!, regular excercise could help with that. Here's a free 30 day membership, from our partners at Fitness First."}),
      words: ["Feeling down", "no energy", "wake up"]
    },
    {
      name: 'gp',
      reccomendation: new ReccomendationModel({img: '../assets/partners/HotdocLogo.jpg', content: "We think a healthcare professional could help with that. Would you like to book an appointment?"}),
      words: ["suicide", "depressed", "depression", "no hope", "Can't deal", "cant deal"]
    },
  ];


  constructor(public http: HttpClient) {
    console.log('Hello ReccomendationsProvider Provider');
  }


  checkForReccomendation(msg){
    let foundReccomendations = [];
    for(let rec of this.reccomendationsMap){
        for(let word of rec.words){
          if(this.findWordsAndPhrases(msg,word)){
            foundReccomendations.push(rec.reccomendation);
            console.log("Matched found:"+word);
          }
        }
    }
    return foundReccomendations;
  }


  private findWordsAndPhrases(msg:string, matchWith:string): boolean{
    let regExPattern = "\\b("+(matchWith.toLowerCase())+")\\b";
    let regEx = new RegExp(regExPattern,'g');
    return regEx.test(msg.toLowerCase());
  }

}
