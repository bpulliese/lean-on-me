import {User} from "./user.model";

export class Session {
  id: string;
  participants: User[] = [];
  constructor(session?:Session){
    Object.assign(this,session);
  }
}
