import { IUser } from "./user.model";

export interface matchFinderRequest
{
  user: any[],
  peers: { [nickname: string] : any[]}
}
