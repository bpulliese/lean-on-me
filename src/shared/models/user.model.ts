
export enum StressType{
  Financials,
  Parenthood,
  WorkRelatedStress,
  Bullying,
  SocialIsolation,
  DontKnow,
  Other
}

export enum Avatar{
  Dog,
  Dog2,
  Cat,
  Bird
}


export interface IUser {
  firstname:string;
  surname:string;
  nickname: string;
  stresstypes: StressType[];
  maritalstatus: string;
  avatar: Avatar;
  anonymous: boolean;
  realpeeps: boolean;
  hcp:boolean;
  chatbot:boolean;
  age:number;
  sex:string;
  email: string;
  password: string;
}




export class User implements IUser{
  email: string;
  password: string;
  firstname:string;
  surname:string;
  nickname: string;
  stresstypes: StressType[] = [];
  maritalstatus: string;
  avatar: Avatar;
  anonymous: boolean = true;
  realpeeps: boolean = true;
  hcp:boolean = true;
  chatbot:boolean = true;
  age:number;
  sex:string;
  constructor(user?:User){
    Object.assign(this,user);
  }
}
