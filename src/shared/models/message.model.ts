import {User} from "./user.model";
import {ReccomendationModel} from "./reccomendation.model";

export class MessageModel {
  msg: string;
  peer: User;
  date: Number = Date.now();
  reccomendations: ReccomendationModel[] = [];
  constructor(data?: MessageModel){
    Object.assign(this, data);
  }
}
